# Пример проекта по курсу "Базы Данных", весна 2022

## Обзор репозитория

В репозитории в вашими проектами также должна быть простая система для запуска ваших скриптов и проверки их 
корректности на уровне работает/не работает. Также это необходимо для удобства проверяющего `==` более оперативная 
обратная связь.  

Ваши скрипты прогоняются через поднятую базу данных в CI (см. раздел `"БД"` ниже).

### Основные компоненты репозитория

Для полноценного запуска CI с Вашими скриптами, запросами необходимы:

* `workflows.yaml` -- определяет что и в какой последовательности запускать, **надо править содержимое** 
* `.gitlab-ci.yml` -- описание CI, **не надо править содержимое**
* `runner` -- исходники для запуска БД и исполнение ваших скриптов, **не надо править содержимое**  

которые которые должны лежать в корне репозитория. Рекомендуется проект хранить в отдельной директории нежели "куча 
файлов в корне".

### Структура файла workflows.yaml

Cостоит из т.н. workflow, пример:
```yaml
comment: '...'
before_all:  # описываем таски, которые выполнятся единожды перед всеми тасками из блока main_tasks
  - comment: 'Install dependencies'
    subtasks:
      - type: 'shell'
        file_location: './project/install.sh'
before_each:  # описываем таски, которые выполнятся перед каждой таской из блока main_tasks
  - comment: 'Echo hello in console'
    subtasks:
      - type: 'shell'
        file_location: './project/scripts/bash/hello.sh'
main_tasks:  # основные таски-тесты
  - comment: 'Queries from file "task1.sql".'
    subtasks:
       - type: 'simple_sql'
         file_location: './project/sql/task1.sql'
       - type: 'complex_sql'
         file_location: './project/sql/ddl.sql'
       - type: 'simple_sql'
         file_location: './project/sql/select.sql'
  - comment: 'Echo hello and goodbye'
    subtasks:
      - type: 'shell'
        file_location: './project/scripts/bash/hello.sh'
      - type: 'shell'
        file_location: './project/scripts/bash/goodbye.sh'
after_each:  # описываем таски, которые выполнятся после каждой таски из блока main_tasks
  - comment: 'Echo goodbye in console'
    subtasks:
      - type: 'shell'
        file_location: './project/scripts/bash/goodbye.sh'
after_all:  # описываем таски, которые выполнятся единожды после всех тасок из блока main_tasks
  - ...
---
# можно описывать несколько workflow, разделяя их ---

comment: 'Another workflow'
before_all:
  - ...
before_each:
  - ...
main_tasks:
  - comment: 'Task from another workflow'
    subtasks:
      - type: 'simple_sql'
        file_location: 'query.sql'
      - ...
  - ...
after_each:
  - ...
after_all:
  - ...
```
Каждый workflow состоит из блоков:
1. `comment` (опционально)
2. `before_all` (опционально)
3. `before_each` (опционально)
4. `main_tasks` (обязательно), результат работы sql-запросов и cкриптов будут выводиться в логах
5. `after_all` (опционально)
6. `after_each` (опционально)

Минимальный `workflows.yaml` имеет вид:
```yaml
comment: '...'
main_tasks: []
```

Блоки №2-№6 состоят тасков, которые разбиты на субтаски.
Таски и субтаски выполняются как указано на схемах ниже.

![](./pics/workflow.png)
![](./pics/tasks.png)

Каждая субтаска должна содержать свой тип (1 из 3) и местоположение (относительно файла `workflows.yaml`):
```yaml
subtasks:
  - type: 'simple_sql' | 'complex_sql' | 'shell'
    file_location: <RELATIVE_PATH>
  - ...
```

Типы субтасок:
* `simple_sql` -- последовательное выполнение одного или нескольких sql-запросов в раздельных пользовательских сессиях
* `complex_sql` -- последовательное выполнение одного или нескольких sql-запросов в одной пользовательской сессии (подходит для описания триггеров, хранимых процедур, транзакций, ...)
* `shell` -- запуск чего-либо в терминале: bash-скрипты, python-скрипты, ..., также можно доскачивать нужные пакеты и языки программирования

### БД

В рамках CI поднимается docker-контейнер с Postgres 12 (образ `ubuntu/postgres:12-20.04_beta`) с:
 * именем пользователя: `postgres`
 * паролем: `postgres`
 * именем базы: `org_mipt_atp_db`
 * хостом: `docker`
 * портом: `49154`  

Порт может меняться, см. строку в CI: 
```bash
RunnerTest > test() STANDARD_OUT
    JDBC URL for container: jdbc:postgresql://docker:49154/org_mipt_atp_db?loggerLevel=OFF
```

### P.S.

* Примеры sql-запросов, shell-скриптов или запуска программ на Python можно посмотреть в директории `project` в этом 
репозитории.
* Ваши схемы и рисунки лучше оформлять в одном `README.md` в корне репозитория. То же самое касается словесного 
  описания 
  самого проекта.
